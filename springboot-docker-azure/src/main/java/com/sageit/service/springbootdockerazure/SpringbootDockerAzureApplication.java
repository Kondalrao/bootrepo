package com.sageit.service.springbootdockerazure;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootDockerAzureApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootDockerAzureApplication.class, args);
	}

}
