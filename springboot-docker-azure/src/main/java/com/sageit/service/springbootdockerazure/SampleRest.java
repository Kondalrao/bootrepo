package com.sageit.service.springbootdockerazure;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value="/sayhello")
public class SampleRest {
	
	@GetMapping
	public String sayHello() {
		return "Hey .. Hello";
	}

}
